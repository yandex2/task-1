#!/usr/bin/env bash

SRC_PATH='/var/log/yandex'
DST_PATH="/backup/$(hostname | cut -d. -f1)"
R_SERVER='backup.yandex.ru'


if cd "${SRC_PATH}"; then
  find . -name '*.log.1' -print0 | rsync -0 -az --files-from=- . "${R_SERVER}":"${DST_PATH}"
  ssh "${R_SERVER}" \
    'find '"${DST_PATH}"' -name "*.log.1" | while read -r file; do date=$(date +%Y%m%d --date=@$(stat -c%Y "${file}")); mv "${file}"{,."${date}"}; done'
else
  echo "${SRC_PATH} is unavailable" >&2
  exit 1
fi
