#!/usr/bin/env bash

SRC_PATH='/var/log/yandex'
DST_PATH="/backup/$(hostname | cut -d. -f1)"
R_SERVER='backup.yandex.ru'


if cd "${SRC_PATH}"; then
  src_logs=$(find . -name '*.log.1' -print0 | xargs -0 md5sum)
  dst_logs=$(ssh "${R_SERVER}" 'find '"${DST_PATH}"' -name "*.log.*" -exec dirname {} \;| sort -u | while read -r dir; do ls -1t "${dir}"/*.log.* | head -1 | xargs -I{} md5sum {}; done')
  comm -1 <<<"${src_logs}" <<<"${dst_logs}"
else
  echo "${SRC_PATH} is unavailable" >&2
  exit 1
fi
